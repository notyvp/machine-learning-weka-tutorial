package edu.rupp.mite.w13;
/**
 * 
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * @author notys
 *
 */
public class SimplePaintPanelGUI extends JPanel implements MouseListener, MouseMotionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4508809120597954104L;

	private Graphics graphicsForDrawing; // A graphics context for the panel
	private boolean dragging; // This is set to true while the user is drawing.
	/*
	 * The following variables are used when the user is sketching a curve while
	 * dragging a mouse.
	 */

	private int prevX, prevY; // The previous location of the mouse.

	public SimplePaintPanelGUI() {
		super();
		setBackground(Color.WHITE);
		addMouseListener(this);
		addMouseMotionListener(this);
	}

	/**
	 * This routine is called in mousePressed when the user clicks on the drawing
	 * area. It sets up the graphics context, graphicsForDrawing, to be used to draw
	 * the user's sketch in the current color.
	 */
	private void setUpDrawingGraphics() {
		graphicsForDrawing = getGraphics();
		graphicsForDrawing.setColor(Color.BLACK);
	} // end setUpDrawingGraphics()

	@Override
	public void mouseDragged(MouseEvent evt) {
		if (dragging == false)
			return; // Nothing to do because the user isn't drawing.

		int x = evt.getX(); // x-coordinate of mouse.
		int y = evt.getY(); // y-coordinate of mouse.

		if (x < 3) // Adjust the value of x,
			x = 3; // to make sure it's in
		if (x > getWidth() - 4) // the drawing area.
			x = getWidth() - 4;

		if (y < 3) // Adjust the value of y,
			y = 3; // to make sure it's in
		if (y > getHeight() - 4) // the drawing area.
			y = getHeight() - 4;
		graphicsForDrawing.fillOval(prevX, prevY, 12, 12);

		prevX = x; // Get ready for the next line segment in the curve.
		prevY = y;
	}// end mouseDragged()

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent evt) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent evt) {
		int x = evt.getX(); // x-coordinate where the user clicked.
		int y = evt.getY(); // y-coordinate where the user clicked.

		if (dragging == true) // Ignore mouse presses that occur
			return; // when user is already drawing a curve.
					// (This can happen if the user presses
					// two mouse buttons at the same time.)
		prevX = x;
		prevY = y;
		dragging = true;
		setUpDrawingGraphics();
	}

	public void save(String imageFile) {
		Rectangle r = getBounds();
		try {
			BufferedImage i = new BufferedImage(r.width, r.height, BufferedImage.TYPE_BYTE_BINARY);
			Graphics2D g = i.createGraphics();
			paint(g);
			ImageIO.write(i, "png", new File(imageFile));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (dragging == false)
			return; // Nothing to do because the user isn't drawing.
		dragging = false;
		//graphicsForDrawing.dispose();
		//graphicsForDrawing = null;

	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	}

}
