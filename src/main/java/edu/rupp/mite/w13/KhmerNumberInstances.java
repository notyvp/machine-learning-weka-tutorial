/**
 * 
 */
package edu.rupp.mite.w13;

import java.util.Enumeration;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 * @author notys
 *
 */
public class KhmerNumberInstances implements Instance {

	/* (non-Javadoc)
	 * @see weka.core.Copyable#copy()
	 */
	@Override
	public Object copy() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#attribute(int)
	 */
	@Override
	public Attribute attribute(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#attributeSparse(int)
	 */
	@Override
	public Attribute attributeSparse(int indexOfIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#classAttribute()
	 */
	@Override
	public Attribute classAttribute() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#classIndex()
	 */
	@Override
	public int classIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#classIsMissing()
	 */
	@Override
	public boolean classIsMissing() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#classValue()
	 */
	@Override
	public double classValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#copy(double[])
	 */
	@Override
	public Instance copy(double[] values) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#dataset()
	 */
	@Override
	public Instances dataset() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#deleteAttributeAt(int)
	 */
	@Override
	public void deleteAttributeAt(int position) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#enumerateAttributes()
	 */
	@Override
	public Enumeration<Attribute> enumerateAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#equalHeaders(weka.core.Instance)
	 */
	@Override
	public boolean equalHeaders(Instance inst) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#equalHeadersMsg(weka.core.Instance)
	 */
	@Override
	public String equalHeadersMsg(Instance inst) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#hasMissingValue()
	 */
	@Override
	public boolean hasMissingValue() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#index(int)
	 */
	@Override
	public int index(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#insertAttributeAt(int)
	 */
	@Override
	public void insertAttributeAt(int position) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#isMissing(int)
	 */
	@Override
	public boolean isMissing(int attIndex) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#isMissingSparse(int)
	 */
	@Override
	public boolean isMissingSparse(int indexOfIndex) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#isMissing(weka.core.Attribute)
	 */
	@Override
	public boolean isMissing(Attribute att) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#mergeInstance(weka.core.Instance)
	 */
	@Override
	public Instance mergeInstance(Instance inst) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#numAttributes()
	 */
	@Override
	public int numAttributes() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#numClasses()
	 */
	@Override
	public int numClasses() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#numValues()
	 */
	@Override
	public int numValues() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#replaceMissingValues(double[])
	 */
	@Override
	public void replaceMissingValues(double[] array) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setClassMissing()
	 */
	@Override
	public void setClassMissing() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setClassValue(double)
	 */
	@Override
	public void setClassValue(double value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setClassValue(java.lang.String)
	 */
	@Override
	public void setClassValue(String value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setDataset(weka.core.Instances)
	 */
	@Override
	public void setDataset(Instances instances) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setMissing(int)
	 */
	@Override
	public void setMissing(int attIndex) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setMissing(weka.core.Attribute)
	 */
	@Override
	public void setMissing(Attribute att) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setValue(int, double)
	 */
	@Override
	public void setValue(int attIndex, double value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setValueSparse(int, double)
	 */
	@Override
	public void setValueSparse(int indexOfIndex, double value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setValue(int, java.lang.String)
	 */
	@Override
	public void setValue(int attIndex, String value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setValue(weka.core.Attribute, double)
	 */
	@Override
	public void setValue(Attribute att, double value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setValue(weka.core.Attribute, java.lang.String)
	 */
	@Override
	public void setValue(Attribute att, String value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#setWeight(double)
	 */
	@Override
	public void setWeight(double weight) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#relationalValue(int)
	 */
	@Override
	public Instances relationalValue(int attIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#relationalValue(weka.core.Attribute)
	 */
	@Override
	public Instances relationalValue(Attribute att) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#stringValue(int)
	 */
	@Override
	public String stringValue(int attIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#stringValue(weka.core.Attribute)
	 */
	@Override
	public String stringValue(Attribute att) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#toDoubleArray()
	 */
	@Override
	public double[] toDoubleArray() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#toStringNoWeight(int)
	 */
	@Override
	public String toStringNoWeight(int afterDecimalPoint) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#toStringNoWeight()
	 */
	@Override
	public String toStringNoWeight() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#toStringMaxDecimalDigits(int)
	 */
	@Override
	public String toStringMaxDecimalDigits(int afterDecimalPoint) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#toString(int, int)
	 */
	@Override
	public String toString(int attIndex, int afterDecimalPoint) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#toString(int)
	 */
	@Override
	public String toString(int attIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#toString(weka.core.Attribute, int)
	 */
	@Override
	public String toString(Attribute att, int afterDecimalPoint) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#toString(weka.core.Attribute)
	 */
	@Override
	public String toString(Attribute att) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#value(int)
	 */
	@Override
	public double value(int attIndex) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#valueSparse(int)
	 */
	@Override
	public double valueSparse(int indexOfIndex) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#value(weka.core.Attribute)
	 */
	@Override
	public double value(Attribute att) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see weka.core.Instance#weight()
	 */
	@Override
	public double weight() {
		// TODO Auto-generated method stub
		return 0;
	}

}
