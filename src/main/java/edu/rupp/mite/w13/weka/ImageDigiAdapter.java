/**
 * 
 */
package edu.rupp.mite.w13.weka;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.Arrays;

import javax.imageio.ImageIO;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import weka.core.Instance;
import weka.core.Instances;

/**
 * @author sovannoty
 *
 */
public class ImageDigiAdapter {

	private VelocityEngine ve = null;
	private Template t = null;
	private static ImageDigiAdapter instance = null;

	private ImageDigiAdapter() throws URISyntaxException {

	}

	public static ImageDigiAdapter getInstance() throws URISyntaxException {
		if (instance == null) {
			instance = new ImageDigiAdapter();
		}
		return instance;
	}

	public int[][] getMatrixOfImageFromFile(File file) throws IOException {
		BufferedImage image = ImageIO.read(file);
		return getMatrixOfImage(image);
	}

	public int[][] getMatrixOfImage(BufferedImage bufferedImage) {
		int width = bufferedImage.getWidth(null);
		int height = bufferedImage.getHeight(null);
		int[][] pixels = new int[width][height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int value = bufferedImage.getData().getSample(j, i, 0);
				pixels[i][j] = value <= 0 ? 1 : 0;
			}
			System.out.println(Arrays.toString(pixels[i]));
		}
		return pixels;
	}

}
