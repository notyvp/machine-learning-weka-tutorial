/**
 * 
 */
package edu.rupp.mite.w13.weka;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import weka.core.Instance;
import weka.core.Instances;

/**
 * @author sovannoty
 *
 */
public class DigiInstancetAdapter {

	private VelocityEngine ve = null;
	private Template t = null;
	private static DigiInstancetAdapter instance = null;

	private DigiInstancetAdapter() throws URISyntaxException {
		ve = new VelocityEngine();
		ve.init();
		t = ve.getTemplate("src\\main\\resources\\digi-arff.vm");
	}

	public static DigiInstancetAdapter getInstance() throws URISyntaxException {
		if (instance == null) {
			instance = new DigiInstancetAdapter();
		}
		return instance;
	}

	public Instance digitToInstances(Digit digit) throws IOException {
		/* create a context and add data */
		VelocityContext context = new VelocityContext();
		context.put("d", digit);
		/* now render the template into a StringWriter */
		StringWriter writer = new StringWriter();
		t.merge(context, writer);
		Reader inputString = new StringReader(writer.toString());
		BufferedReader reader = new BufferedReader(inputString);
		Instances data = new Instances(reader);
		data.setClassIndex(data.numAttributes() - 1);
		return data.firstInstance();
	}

	public int[][] getMatrixOfImageFromFile(File file) throws IOException {
		BufferedImage image = ImageIO.read(file);
		return getMatrixOfImage(image);
	}

	public int[][] getMatrixOfImage(BufferedImage bufferedImage) {
		int width = bufferedImage.getWidth(null);
		int height = bufferedImage.getHeight(null);
		int[][] pixels = new int[width][height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				pixels[i][j] = bufferedImage.getRGB(i, j);
			}
		}

		return pixels;
	}

}
