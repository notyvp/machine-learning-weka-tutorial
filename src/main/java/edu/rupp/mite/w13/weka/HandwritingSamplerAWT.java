/**
 * 
 */
package edu.rupp.mite.w13.weka;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import edu.rupp.mite.w13.weka.algorithms.KNearestNeighborAlgorithm;

public class HandwritingSamplerAWT {
    static JFrame frame;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                CreateAndShowGUI(); 
            }
        });
    }

    private static void CreateAndShowGUI() {
        frame = new JFrame("Writing Surface v0.1");
        frame.getContentPane().setLayout(new FlowLayout(FlowLayout.RIGHT));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBackground(Color.LIGHT_GRAY);
        frame.setBounds(100, 100, 450, 300);
        frame.pack();
        frame.add(new MyPanel());
        frame.setVisible(true);
    }
}

class MyPanel extends JPanel{
    private int x,y;
    static int strokeIndex;
    private long reducedMillis;
    private ArrayList<StrokeInfo> strokes;
    private JButton btnSave;



    public MyPanel() {
        MyPanel.strokeIndex=0;
        this.reducedMillis = 1435800000000L;
        this.strokes = new ArrayList<>();
        this.btnSave = new JButton("SAVE SAMPLE");
        this.btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
					WriteCoordinates();
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        this.add(this.btnSave);

        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                x=e.getX();
                y=e.getY();
                SaveCoordinates(x,y,"PRESSED");
                repaint();
            }
        });

        addMouseMotionListener(new MouseAdapter() {
            public void mouseDragged(MouseEvent e) {
                x=e.getX();
                y=e.getY();
                SaveCoordinates(x,y,"DRAGGED");
                repaint();
            }
        });

        addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent e) {
                x=e.getX();
                y=e.getY();
                SaveCoordinates(x,y,"RELEASED");
                repaint();
            }
        });
    }

    void SaveCoordinates(int xCoordinate, int yCoordinate, String actionIndicator){
        try {
            Calendar cal = Calendar.getInstance();
            Date currDate = cal.getTime();
            double timeStamp=(double)(currDate.getTime()-reducedMillis);
            PointInfo pointObj = new PointInfo(xCoordinate, yCoordinate, timeStamp);
            switch (actionIndicator) {
                case "PRESSED":
                    StrokeInfo newStroke = new StrokeInfo();
                    newStroke.points.add(pointObj);
                    strokes.add(newStroke);
                    break;
                case "DRAGGED":
                    strokes.get(strokeIndex).points.add(pointObj);
                    break;
                case "RELEASED":
                    strokeIndex+=1;
                    break;
            }
        } catch (Exception ex){
            String errMsg = ex.getMessage();
            System.out.println(errMsg);
        }
    }

    void WriteCoordinates() throws URISyntaxException {
        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            String currTimeString = dateFormat.format(cal.getTime());

            DecimalFormat decFormat = new DecimalFormat("#");
            decFormat.setMaximumFractionDigits(2);
            File file = new File("HandwritingText-"+currTimeString+".txt");
            FileWriter writer = new FileWriter(file);
            SetStrokeAttributes(strokes);
            ListIterator<PointInfo> pointItr;
            if (strokes.isEmpty()==false) {
                for (int index = 0; index < strokeIndex; index++) {
                    writer.write(strokes.get(index).colour);
                    writer.append('\t');
                    writer.write(decFormat.format( strokes.get(index).startTime));
                    writer.append('\t');
                    writer.write(decFormat.format( strokes.get(index).endTime));
                    writer.append('\n');
                    pointItr = strokes.get(index).points.listIterator();
                    while (pointItr.hasNext()) {
                        PointInfo currPoint = pointItr.next();
                        writer.write(String.valueOf(currPoint.x));
                        writer.append('\t');
                        writer.write(String.valueOf(currPoint.y));
                        writer.append('\t');
                        writer.write(decFormat.format(currPoint.timestamp));
                        writer.append('\n');
                    }
                    writer.append('#');
                    writer.append('\n');
                }
            }
            writer.close();

            SaveScreenshot(file.getAbsolutePath()+".png");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    void SetStrokeAttributes(ArrayList<StrokeInfo> strokeList) {
        double startTime, endTime;
        String colour;
        StrokeInfo tmpStroke;
        ArrayList<PointInfo> points;
        if (strokeList.isEmpty() == false) {
            for (int index = 0; index < strokeList.size(); index++) {
                tmpStroke = strokeList.get(index);
                points = tmpStroke.points;
                tmpStroke.colour = "black";
                tmpStroke.startTime=points.get(0).timestamp;
                tmpStroke.endTime=points.get(points.size()-1).timestamp;
                strokeList.set(index, tmpStroke);
            }
        }
    }

    void SaveScreenshot(String imgFilePath){
        try {
            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage capture = new Robot().createScreenCapture(screenRect);
            ImageIO.write(capture, "png", new File(imgFilePath));
        } catch (IOException | AWTException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public Dimension getPreferredSize() {
        return new Dimension(1366,768);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponents(g);
        g.setColor(Color.BLACK);
        g.drawLine(x, y, x, y);
    }
}

class PointInfo {
    int x,y;
    double timestamp;

    public PointInfo(int px, int py, double ts) {
        this.x=px;
        this.y=py;
        this.timestamp=ts;
    }
}

class StrokeInfo {
    ArrayList<PointInfo> points;
    double startTime, endTime;
    String colour;
    public StrokeInfo() {
        points= new ArrayList<>();
    }
}
