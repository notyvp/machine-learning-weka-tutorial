/**
 * 
 */
package edu.rupp.mite.w13.weka;

/**
 * @author sovannoty
 *
 */
public class HDDigit extends Digit {

	String input65;
	String input66;
	String input67;
	String input68;
	String input69;
	String input70;
	String input71;
	String input72;
	String input73;
	String input74;
	String input75;
	String input76;
	String input77;
	String input78;
	String input79;
	String input80;
	String input81;
	String input82;
	String input83;
	String input84;
	String input85;
	String input86;
	String input87;
	String input88;
	String input89;
	String input90;
	String input91;
	String input92;
	String input93;
	String input94;
	String input95;
	String input96;
	String input97;
	String input98;
	String input99;
	String input100;
	String input101;
	String input102;
	String input103;
	String input104;
	String input105;
	String input106;
	String input107;
	String input108;
	String input109;
	String input110;
	String input111;
	String input112;
	String input113;
	String input114;
	String input115;
	String input116;
	String input117;
	String input118;
	String input119;
	String input120;
	String input121;
	String input122;
	String input123;
	String input124;
	String input125;
	String input126;
	String input127;
	String input128;

	public HDDigit() {

	}
	
	public void init(String[] array) {
		input65 = array[64];
		input66 = array[65];
		input67 = array[66];
		input68 = array[67];
		input69 = array[68];
		input70 = array[69];
		input71 = array[70];
		input72 = array[71];
		input73 = array[72];
		input74 = array[73];
		input75 = array[74];
		input76 = array[75];
		input77 = array[76];
		input78 = array[77];
		input79 = array[78];
		input80 = array[79];
		input81 = array[80];
		input82 = array[81];
		input83 = array[82];
		input84 = array[83];
		input85 = array[84];
		input86 = array[85];
		input87 = array[86];
		input88 = array[87];
		input89 = array[88];
		input90 = array[89];
		input91 = array[90];
		input92 = array[91];
		input93 = array[92];
		input94 = array[93];
		input95 = array[94];
		input96 = array[95];
		input97 = array[96];
		input98 = array[97];
		input99 = array[98];
		input100 = array[99];
		input101 = array[100];
		input102 = array[101];
		input103 = array[102];
		input104 = array[103];
		input105 = array[104];
		input106 = array[105];
		input107 = array[106];
		input108 = array[107];
		input109 = array[108];
		input110 = array[109];
		input111 = array[110];
		input112 = array[111];
		input113 = array[112];
		input114 = array[113];
		input115 = array[114];
		input116 = array[115];
		input117 = array[116];
		input118 = array[117];
		input119 = array[118];
		input120 = array[119];
		input121 = array[120];
		input122 = array[121];
		input123 = array[122];
		input124 = array[123];
		input125 = array[124];
		input126 = array[125];
		input127 = array[126];
		input128 = array[127];
		super.init(array);
	}

	public HDDigit(String str) {
		String[] array = str.split(",");
		init(array);
		
	}

	public String getInput65() {
		return input65;
	}

	public void setInput65(String input65) {
		this.input65 = input65;
	}

	public String getInput66() {
		return input66;
	}

	public void setInput66(String input66) {
		this.input66 = input66;
	}

	public String getInput67() {
		return input67;
	}

	public void setInput67(String input67) {
		this.input67 = input67;
	}

	public String getInput68() {
		return input68;
	}

	public void setInput68(String input68) {
		this.input68 = input68;
	}

	public String getInput69() {
		return input69;
	}

	public void setInput69(String input69) {
		this.input69 = input69;
	}

	public String getInput70() {
		return input70;
	}

	public void setInput70(String input70) {
		this.input70 = input70;
	}

	public String getInput71() {
		return input71;
	}

	public void setInput71(String input71) {
		this.input71 = input71;
	}

	public String getInput72() {
		return input72;
	}

	public void setInput72(String input72) {
		this.input72 = input72;
	}

	public String getInput73() {
		return input73;
	}

	public void setInput73(String input73) {
		this.input73 = input73;
	}

	public String getInput74() {
		return input74;
	}

	public void setInput74(String input74) {
		this.input74 = input74;
	}

	public String getInput75() {
		return input75;
	}

	public void setInput75(String input75) {
		this.input75 = input75;
	}

	public String getInput76() {
		return input76;
	}

	public void setInput76(String input76) {
		this.input76 = input76;
	}

	public String getInput77() {
		return input77;
	}

	public void setInput77(String input77) {
		this.input77 = input77;
	}

	public String getInput78() {
		return input78;
	}

	public void setInput78(String input78) {
		this.input78 = input78;
	}

	public String getInput79() {
		return input79;
	}

	public void setInput79(String input79) {
		this.input79 = input79;
	}

	public String getInput80() {
		return input80;
	}

	public void setInput80(String input80) {
		this.input80 = input80;
	}

	public String getInput81() {
		return input81;
	}

	public void setInput81(String input81) {
		this.input81 = input81;
	}

	public String getInput82() {
		return input82;
	}

	public void setInput82(String input82) {
		this.input82 = input82;
	}

	public String getInput83() {
		return input83;
	}

	public void setInput83(String input83) {
		this.input83 = input83;
	}

	public String getInput84() {
		return input84;
	}

	public void setInput84(String input84) {
		this.input84 = input84;
	}

	public String getInput85() {
		return input85;
	}

	public void setInput85(String input85) {
		this.input85 = input85;
	}

	public String getInput86() {
		return input86;
	}

	public void setInput86(String input86) {
		this.input86 = input86;
	}

	public String getInput87() {
		return input87;
	}

	public void setInput87(String input87) {
		this.input87 = input87;
	}

	public String getInput88() {
		return input88;
	}

	public void setInput88(String input88) {
		this.input88 = input88;
	}

	public String getInput89() {
		return input89;
	}

	public void setInput89(String input89) {
		this.input89 = input89;
	}

	public String getInput90() {
		return input90;
	}

	public void setInput90(String input90) {
		this.input90 = input90;
	}

	public String getInput91() {
		return input91;
	}

	public void setInput91(String input91) {
		this.input91 = input91;
	}

	public String getInput92() {
		return input92;
	}

	public void setInput92(String input92) {
		this.input92 = input92;
	}

	public String getInput93() {
		return input93;
	}

	public void setInput93(String input93) {
		this.input93 = input93;
	}

	public String getInput94() {
		return input94;
	}

	public void setInput94(String input94) {
		this.input94 = input94;
	}

	public String getInput95() {
		return input95;
	}

	public void setInput95(String input95) {
		this.input95 = input95;
	}

	public String getInput96() {
		return input96;
	}

	public void setInput96(String input96) {
		this.input96 = input96;
	}

	public String getInput97() {
		return input97;
	}

	public void setInput97(String input97) {
		this.input97 = input97;
	}

	public String getInput98() {
		return input98;
	}

	public void setInput98(String input98) {
		this.input98 = input98;
	}

	public String getInput99() {
		return input99;
	}

	public void setInput99(String input99) {
		this.input99 = input99;
	}

	public String getInput100() {
		return input100;
	}

	public void setInput100(String input100) {
		this.input100 = input100;
	}

	public String getInput101() {
		return input101;
	}

	public void setInput101(String input101) {
		this.input101 = input101;
	}

	public String getInput102() {
		return input102;
	}

	public void setInput102(String input102) {
		this.input102 = input102;
	}

	public String getInput103() {
		return input103;
	}

	public void setInput103(String input103) {
		this.input103 = input103;
	}

	public String getInput104() {
		return input104;
	}

	public void setInput104(String input104) {
		this.input104 = input104;
	}

	public String getInput105() {
		return input105;
	}

	public void setInput105(String input105) {
		this.input105 = input105;
	}

	public String getInput106() {
		return input106;
	}

	public void setInput106(String input106) {
		this.input106 = input106;
	}

	public String getInput107() {
		return input107;
	}

	public void setInput107(String input107) {
		this.input107 = input107;
	}

	public String getInput108() {
		return input108;
	}

	public void setInput108(String input108) {
		this.input108 = input108;
	}

	public String getInput109() {
		return input109;
	}

	public void setInput109(String input109) {
		this.input109 = input109;
	}

	public String getInput110() {
		return input110;
	}

	public void setInput110(String input110) {
		this.input110 = input110;
	}

	public String getInput111() {
		return input111;
	}

	public void setInput111(String input111) {
		this.input111 = input111;
	}

	public String getInput112() {
		return input112;
	}

	public void setInput112(String input112) {
		this.input112 = input112;
	}

	public String getInput113() {
		return input113;
	}

	public void setInput113(String input113) {
		this.input113 = input113;
	}

	public String getInput114() {
		return input114;
	}

	public void setInput114(String input114) {
		this.input114 = input114;
	}

	public String getInput115() {
		return input115;
	}

	public void setInput115(String input115) {
		this.input115 = input115;
	}

	public String getInput116() {
		return input116;
	}

	public void setInput116(String input116) {
		this.input116 = input116;
	}

	public String getInput117() {
		return input117;
	}

	public void setInput117(String input117) {
		this.input117 = input117;
	}

	public String getInput118() {
		return input118;
	}

	public void setInput118(String input118) {
		this.input118 = input118;
	}

	public String getInput119() {
		return input119;
	}

	public void setInput119(String input119) {
		this.input119 = input119;
	}

	public String getInput120() {
		return input120;
	}

	public void setInput120(String input120) {
		this.input120 = input120;
	}

	public String getInput121() {
		return input121;
	}

	public void setInput121(String input121) {
		this.input121 = input121;
	}

	public String getInput122() {
		return input122;
	}

	public void setInput122(String input122) {
		this.input122 = input122;
	}

	public String getInput123() {
		return input123;
	}

	public void setInput123(String input123) {
		this.input123 = input123;
	}

	public String getInput124() {
		return input124;
	}

	public void setInput124(String input124) {
		this.input124 = input124;
	}

	public String getInput125() {
		return input125;
	}

	public void setInput125(String input125) {
		this.input125 = input125;
	}

	public String getInput126() {
		return input126;
	}

	public void setInput126(String input126) {
		this.input126 = input126;
	}

	public String getInput127() {
		return input127;
	}

	public void setInput127(String input127) {
		this.input127 = input127;
	}

	public String getInput128() {
		return input128;
	}

	public void setInput128(String input128) {
		this.input128 = input128;
	}

	
	
	

}
