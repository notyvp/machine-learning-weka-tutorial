/**
 * 
 */
package edu.rupp.mite.w13.weka.algorithms;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;

import weka.classifiers.lazy.IBk;
import weka.core.Instance;
import weka.core.Instances;

/**
 * @author notys
 * k-nearest neighbor algorithm
 */
public class KNearestNeighborAlgorithm {

	private IBk ibk;
	private static KNearestNeighborAlgorithm instance;

	private KNearestNeighborAlgorithm() throws Exception {
		// build classifier
		Instances data = getData("/optdigits.arff", 1);
		this.ibk = new IBk();
		this.ibk.buildClassifier(data);
	}
	
	static public KNearestNeighborAlgorithm getInstance() throws Exception {
		if(instance == null) {
			instance = new KNearestNeighborAlgorithm();
		}
		return instance;
	}

	public Integer prediction(Instance instance) throws Exception {
		double kNearestNeighborResult = this.ibk.classifyInstance(instance);
		System.out.print(" k-nearest neighbor algorithmResult = " + (int) kNearestNeighborResult);
		return Integer.valueOf(String.valueOf(kNearestNeighborResult));
	}

	private Instances getData(String filename, Integer posClass) throws IOException, URISyntaxException {
		File file = new File(KNearestNeighborAlgorithm.class.getResource(filename).toURI());
		BufferedReader inputReader = new BufferedReader(new FileReader(file));
		Instances data = new Instances(inputReader);
		data.setClassIndex(data.numAttributes() - posClass);
		return data;
	}

}
