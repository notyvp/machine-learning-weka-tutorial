/**
 * 
 */
package edu.rupp.mite.w13.weka;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.SwingUtilities;

import weka.classifiers.lazy.IBk;
import weka.classifiers.rules.ZeroR;
import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;
import weka.gui.treevisualizer.TreeVisualizer;

/**
 * @author notys
 *
 */
public class KNearestNeighborAlgorithmsStarter {

	public void testWekaJ48() throws Exception {
		Instances data = getData("/optdigits.arff", 1);
		//k-nearest neighbor algorithm
		IBk ibk = new IBk();
		// build classifier
		ibk.buildClassifier(data);
		// testData
		Instances newData = getData("/optdigits-test.arff", 1);
		int numAttributes = newData.numAttributes();
		int numInstances = newData.numInstances();
		for (int i = 0; i < numInstances; i++) {
			Instance instance = newData.instance(i);
			double kNearestNeighborResult = ibk.classifyInstance(instance);
			System.out.print("class = " + (instance.value(numAttributes - 1)));
			System.out.println(" k-nearest neighbor algorithmResult = " + (int) kNearestNeighborResult);
			
		}
		
		Digit digit = new Digit("0,1,6,15,12,1,0,0,0,7,16,6,6,10,0,0,0,8,16,2,0,11,2,0,0,5,16,3,0,5,7,0,0,7,13,3,0,8,7,0,0,4,12,0,1,13,5,0,0,0,14,9,15,9,0,0,0,0,6,14,7,1,0,0,0");
		Instance instance = DigiInstancetAdapter.getInstance().digitToInstances(digit);
		double kNearestNeighborResult = ibk.classifyInstance(instance);
		System.out.println("From class k-nearest neighbor algorithmResult = " + (int) kNearestNeighborResult);
		
		// display classifier
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				TreeVisualizer visualizeTree = null;
				try {
//					visualizeTree = new TreeVisualizer(null, decisionTree.graph(), new PlaceNode2());
//					final JFrame jFrame = new JFrame("Weka J48 Klassisfikator: Entscheidungsbaum");
//					jFrame.setSize(600, 500);
//					jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//					jFrame.getContentPane().add(visualizeTree);
//					jFrame.setVisible(true);
//					visualizeTree.fitToScreen();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	private Instances getData(String filename, Integer posClass) throws IOException, URISyntaxException {
		File file = new File(KNearestNeighborAlgorithmsStarter.class.getResource(filename).toURI());
		BufferedReader inputReader = new BufferedReader(new FileReader(file));
		Instances data = new Instances(inputReader);
		data.setClassIndex(data.numAttributes() - posClass);
		return data;
	}

	public static void main(String[] args) throws Exception {
		KNearestNeighborAlgorithmsStarter knearestNeighborAlgorithms = new KNearestNeighborAlgorithmsStarter();
		knearestNeighborAlgorithms.testWekaJ48();
	}

}
