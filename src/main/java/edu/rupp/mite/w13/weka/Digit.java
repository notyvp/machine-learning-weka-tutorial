/**
 * 
 */
package edu.rupp.mite.w13.weka;

/**
 * @author sovannoty
 *
 */
public class Digit {
	
	String clazz;

	String input01;
	String input02;
	String input03;
	String input04;
	String input05;
	String input06;
	String input07;
	String input08;
	String input09;
	String input10;
	String input11;
	String input12;
	String input13;
	String input14;
	String input15;
	String input16;
	String input17;
	String input18;
	String input19;
	String input20;
	String input21;
	String input22;
	String input23;
	String input24;
	String input25;
	String input26;
	String input27;
	String input28;
	String input29;
	String input30;
	String input31;
	String input32;
	String input33;
	String input34;
	String input35;
	String input36;
	String input37;
	String input38;
	String input39;
	String input40;
	String input41;
	String input42;
	String input43;
	String input44;
	String input45;
	String input46;
	String input47;
	String input48;
	String input49;
	String input50;
	String input51;
	String input52;
	String input53;
	String input54;
	String input55;
	String input56;
	String input57;
	String input58;
	String input59;
	String input60;
	String input61;
	String input62;
	String input63;
	String input64;

	public Digit() {

	}

	public Digit(String str) {
		String[] ar = str.split(",");
		init(ar);
	}
	
	public void init(String[] ar) {
		input01 = ar[0];
		input02 = ar[1];
		input03 = ar[2];
		input04 = ar[3];
		input05 = ar[4];
		input06 = ar[5];
		input07 = ar[6];
		input08 = ar[7];
		input09 = ar[8];
		input10 = ar[9];
		input11 = ar[10];
		input12 = ar[11];
		input13 = ar[12];
		input14 = ar[13];
		input15 = ar[14];
		input16 = ar[15];
		input17 = ar[16];
		input18 = ar[17];
		input19 = ar[18];
		input20 = ar[19];
		input21 = ar[20];
		input22 = ar[21];
		input23 = ar[22];
		input24 = ar[23];
		input25 = ar[24];
		input26 = ar[25];
		input27 = ar[26];
		input28 = ar[27];
		input29 = ar[28];
		input30 = ar[29];
		input31 = ar[30];
		input32 = ar[31];
		input33 = ar[32];
		input34 = ar[33];
		input35 = ar[34];
		input36 = ar[35];
		input37 = ar[36];
		input38 = ar[37];
		input39 = ar[38];
		input40 = ar[39];
		input41 = ar[40];
		input42 = ar[41];
		input43 = ar[42];
		input44 = ar[43];
		input45 = ar[44];
		input46 = ar[45];
		input47 = ar[46];
		input48 = ar[47];
		input49 = ar[48];
		input50 = ar[49];
		input51 = ar[50];
		input52 = ar[51];
		input53 = ar[52];
		input54 = ar[53];
		input55 = ar[54];
		input56 = ar[55];
		input57 = ar[56];
		input58 = ar[57];
		input59 = ar[58];
		input60 = ar[59];
		input61 = ar[60];
		input62 = ar[61];
		input63 = ar[62];
		input64 = ar[63];

	}
	
	

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public String getInput01() {
		return input01;
	}

	public void setInput01(String input01) {
		this.input01 = input01;
	}

	public String getInput02() {
		return input02;
	}

	public void setInput02(String input02) {
		this.input02 = input02;
	}

	public String getInput03() {
		return input03;
	}

	public void setInput03(String input03) {
		this.input03 = input03;
	}

	public String getInput04() {
		return input04;
	}

	public void setInput04(String input04) {
		this.input04 = input04;
	}

	public String getInput05() {
		return input05;
	}

	public void setInput05(String input05) {
		this.input05 = input05;
	}

	public String getInput06() {
		return input06;
	}

	public void setInput06(String input06) {
		this.input06 = input06;
	}

	public String getInput07() {
		return input07;
	}

	public void setInput07(String input07) {
		this.input07 = input07;
	}

	public String getInput08() {
		return input08;
	}

	public void setInput08(String input08) {
		this.input08 = input08;
	}

	public String getInput09() {
		return input09;
	}

	public void setInput09(String input09) {
		this.input09 = input09;
	}

	public String getInput10() {
		return input10;
	}

	public void setInput10(String input10) {
		this.input10 = input10;
	}

	public String getInput11() {
		return input11;
	}

	public void setInput11(String input11) {
		this.input11 = input11;
	}

	public String getInput12() {
		return input12;
	}

	public void setInput12(String input12) {
		this.input12 = input12;
	}

	public String getInput13() {
		return input13;
	}

	public void setInput13(String input13) {
		this.input13 = input13;
	}

	public String getInput14() {
		return input14;
	}

	public void setInput14(String input14) {
		this.input14 = input14;
	}

	public String getInput15() {
		return input15;
	}

	public void setInput15(String input15) {
		this.input15 = input15;
	}

	public String getInput16() {
		return input16;
	}

	public void setInput16(String input16) {
		this.input16 = input16;
	}

	public String getInput17() {
		return input17;
	}

	public void setInput17(String input17) {
		this.input17 = input17;
	}

	public String getInput18() {
		return input18;
	}

	public void setInput18(String input18) {
		this.input18 = input18;
	}

	public String getInput19() {
		return input19;
	}

	public void setInput19(String input19) {
		this.input19 = input19;
	}

	public String getInput20() {
		return input20;
	}

	public void setInput20(String input20) {
		this.input20 = input20;
	}

	public String getInput21() {
		return input21;
	}

	public void setInput21(String input21) {
		this.input21 = input21;
	}

	public String getInput22() {
		return input22;
	}

	public void setInput22(String input22) {
		this.input22 = input22;
	}

	public String getInput23() {
		return input23;
	}

	public void setInput23(String input23) {
		this.input23 = input23;
	}

	public String getInput24() {
		return input24;
	}

	public void setInput24(String input24) {
		this.input24 = input24;
	}

	public String getInput25() {
		return input25;
	}

	public void setInput25(String input25) {
		this.input25 = input25;
	}

	public String getInput26() {
		return input26;
	}

	public void setInput26(String input26) {
		this.input26 = input26;
	}

	public String getInput27() {
		return input27;
	}

	public void setInput27(String input27) {
		this.input27 = input27;
	}

	public String getInput28() {
		return input28;
	}

	public void setInput28(String input28) {
		this.input28 = input28;
	}

	public String getInput29() {
		return input29;
	}

	public void setInput29(String input29) {
		this.input29 = input29;
	}

	public String getInput30() {
		return input30;
	}

	public void setInput30(String input30) {
		this.input30 = input30;
	}

	public String getInput31() {
		return input31;
	}

	public void setInput31(String input31) {
		this.input31 = input31;
	}

	public String getInput32() {
		return input32;
	}

	public void setInput32(String input32) {
		this.input32 = input32;
	}

	public String getInput33() {
		return input33;
	}

	public void setInput33(String input33) {
		this.input33 = input33;
	}

	public String getInput34() {
		return input34;
	}

	public void setInput34(String input34) {
		this.input34 = input34;
	}

	public String getInput35() {
		return input35;
	}

	public void setInput35(String input35) {
		this.input35 = input35;
	}

	public String getInput36() {
		return input36;
	}

	public void setInput36(String input36) {
		this.input36 = input36;
	}

	public String getInput37() {
		return input37;
	}

	public void setInput37(String input37) {
		this.input37 = input37;
	}

	public String getInput38() {
		return input38;
	}

	public void setInput38(String input38) {
		this.input38 = input38;
	}

	public String getInput39() {
		return input39;
	}

	public void setInput39(String input39) {
		this.input39 = input39;
	}

	public String getInput40() {
		return input40;
	}

	public void setInput40(String input40) {
		this.input40 = input40;
	}

	public String getInput41() {
		return input41;
	}

	public void setInput41(String input41) {
		this.input41 = input41;
	}

	public String getInput42() {
		return input42;
	}

	public void setInput42(String input42) {
		this.input42 = input42;
	}

	public String getInput43() {
		return input43;
	}

	public void setInput43(String input43) {
		this.input43 = input43;
	}

	public String getInput44() {
		return input44;
	}

	public void setInput44(String input44) {
		this.input44 = input44;
	}

	public String getInput45() {
		return input45;
	}

	public void setInput45(String input45) {
		this.input45 = input45;
	}

	public String getInput46() {
		return input46;
	}

	public void setInput46(String input46) {
		this.input46 = input46;
	}

	public String getInput47() {
		return input47;
	}

	public void setInput47(String input47) {
		this.input47 = input47;
	}

	public String getInput48() {
		return input48;
	}

	public void setInput48(String input48) {
		this.input48 = input48;
	}

	public String getInput49() {
		return input49;
	}

	public void setInput49(String input49) {
		this.input49 = input49;
	}

	public String getInput50() {
		return input50;
	}

	public void setInput50(String input50) {
		this.input50 = input50;
	}

	public String getInput51() {
		return input51;
	}

	public void setInput51(String input51) {
		this.input51 = input51;
	}

	public String getInput52() {
		return input52;
	}

	public void setInput52(String input52) {
		this.input52 = input52;
	}

	public String getInput53() {
		return input53;
	}

	public void setInput53(String input53) {
		this.input53 = input53;
	}

	public String getInput54() {
		return input54;
	}

	public void setInput54(String input54) {
		this.input54 = input54;
	}

	public String getInput55() {
		return input55;
	}

	public void setInput55(String input55) {
		this.input55 = input55;
	}

	public String getInput56() {
		return input56;
	}

	public void setInput56(String input56) {
		this.input56 = input56;
	}

	public String getInput57() {
		return input57;
	}

	public void setInput57(String input57) {
		this.input57 = input57;
	}

	public String getInput58() {
		return input58;
	}

	public void setInput58(String input58) {
		this.input58 = input58;
	}

	public String getInput59() {
		return input59;
	}

	public void setInput59(String input59) {
		this.input59 = input59;
	}

	public String getInput60() {
		return input60;
	}

	public void setInput60(String input60) {
		this.input60 = input60;
	}

	public String getInput61() {
		return input61;
	}

	public void setInput61(String input61) {
		this.input61 = input61;
	}

	public String getInput62() {
		return input62;
	}

	public void setInput62(String input62) {
		this.input62 = input62;
	}

	public String getInput63() {
		return input63;
	}

	public void setInput63(String input63) {
		this.input63 = input63;
	}

	public String getInput64() {
		return input64;
	}

	public void setInput64(String input64) {
		this.input64 = input64;
	}
	
	

}
