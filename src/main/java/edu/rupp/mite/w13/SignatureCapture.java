package edu.rupp.mite.w13;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.JApplet;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import edu.rupp.mite.w13.weka.ImageDigiAdapter;

/**
 * @author Ashwin
 * 
 */
public class SignatureCapture extends JApplet {

	public BufferedImage image = null;
	private JPanel canvas = new JPanel();
	private JPanel colorPanel = new JPanel();
	private Point lastPos = null;
	private Button captureButton = new Button("Capture");
	private Graphics gc;
	Graphics2D imageG = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.applet.Applet#init()
	 */
	@Override
	public void init() {
		// TODO Auto-generated method stub
		super.init();

		setSize(256, 256);

		image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);

		// get the image Graphics object
		imageG = image.createGraphics();
		colorPanel.setLayout(new GridLayout());
		canvas.setSize(getWidth(), getHeight());

		captureButton.setSize(30, 30);
		getContentPane().add(captureButton);
		getContentPane().add(canvas, BorderLayout.CENTER);
		setVisible(true);

		// get the Graphics Context
		gc = canvas.getGraphics();
		canvas.setBackground(Color.WHITE);
		gc.setColor(Color.GRAY);
		gc.fillRect(0, 0, image.getWidth(), image.getHeight());
		gc.setColor(Color.BLACK);

		imageG.setBackground(Color.WHITE);
		imageG.setColor(Color.GRAY);
		imageG.fillRect(0, 0, image.getWidth(), image.getHeight());
		imageG.setColor(Color.BLACK);

		captureButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent a) {

				Container cp = getContentPane();
				final Component comp = cp.add(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
						new JScrollPane(new JTextArea()), new JScrollPane(new JTextArea())));

				int[][] img = null;
				try {
					img = ImageDigiAdapter.getInstance().getMatrixOfImage((BufferedImage) image);
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
		});

		canvas.addMouseMotionListener(new MouseMotionListener() {
			public void mouseDragged(MouseEvent m) {
				// the mouse(pen) was dragged, so the pixels at coords found in
				// MouseEvent m must be updated with current color
				Point p = m.getPoint();
				gc.fillOval(lastPos.x, lastPos.y, 16, 16);
				imageG.fillOval(lastPos.x, lastPos.y, 16, 16);
				lastPos = p;
			}

			public void mouseMoved(MouseEvent m) {
			}
		});
		canvas.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {
			}

			public void mousePressed(MouseEvent e) {
				lastPos = e.getPoint();
			}

			public void mouseReleased(MouseEvent e) {
				lastPos = null;
			}

			public void mouseEntered(MouseEvent e) {
			}

			public void mouseExited(MouseEvent e) {
			}
		});
	}

	/**
	 * 
	 */
	public SignatureCapture() {
	}

	public static void main(String[] args) {
		SignatureCapture p = new SignatureCapture();
	}
}