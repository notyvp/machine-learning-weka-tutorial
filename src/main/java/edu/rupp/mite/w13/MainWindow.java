package edu.rupp.mite.w13;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JFrame;
import javax.swing.BorderFactory;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import edu.rupp.mite.w13.weka.DigiInstancetAdapter;
import edu.rupp.mite.w13.weka.Digit;
import edu.rupp.mite.w13.weka.ImageDigiAdapter;
import edu.rupp.mite.w13.weka.KNearestNeighborAlgorithmsStarter;
import weka.classifiers.lazy.IBk;
import weka.core.Instance;
import weka.core.Instances;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class MainWindow {

	private JFrame frame;
	private Button captureButton = new Button("Capture");
	private JLabel lblNewLabel = new JLabel("N/A");
	private BufferedImage image = null;
	private Graphics gc = null;
	Graphics2D imageG = null;
	private Point lastPos = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws Exception 
	 */
	public MainWindow() throws Exception {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws Exception 
	 */
	private void initialize() throws Exception {
		frame = new JFrame();
		frame.setBounds(100, 100, 545, 378);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		Instances data = getData("/optdigits.arff", 1);
		//k-nearest neighbor algorithm
		IBk ibk = new IBk();
		// build classifier
		ibk.buildClassifier(data);

		image = new BufferedImage(320, 320, BufferedImage.TYPE_INT_ARGB);

		captureButton.setBounds(340, 11, 179, 40);
		captureButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent a) {
				try {
					//int[][] img = ImageDigiAdapter.getInstance().getMatrixOfImage((BufferedImage) image);
					
					Digit digit = new Digit("0,1,6,15,12,1,0,0,0,7,16,6,6,10,0,0,0,8,16,2,0,11,2,0,0,5,16,3,0,5,7,0,0,7,13,3,0,8,7,0,0,4,12,0,1,13,5,0,0,0,14,9,15,9,0,0,0,0,6,14,7,1,0,0,0");
					Instance instance = DigiInstancetAdapter.getInstance().digitToInstances(digit);
					double kNearestNeighborResult = ibk.classifyInstance(instance);
					
					lblNewLabel.setText(String.valueOf(Double.valueOf(kNearestNeighborResult).intValue()));
					
				} catch (URISyntaxException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		frame.getContentPane().add(captureButton);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 320, 320);
		panel.setSize(320, 320);
		panel.setBorder(BorderFactory.createLineBorder(Color.black));
		panel.setBackground(Color.WHITE);
		frame.getContentPane().add(panel);
		
		
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 71));
		lblNewLabel.setBounds(370, 121, 134, 167);
		frame.getContentPane().add(lblNewLabel);
		
		Button button = new Button("Register");
		button.setActionCommand("Register");
		button.setBounds(340, 57, 125, 40);
		frame.getContentPane().add(button);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 20));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}));
		comboBox.setBounds(472, 57, 47, 40);
		frame.getContentPane().add(comboBox);
		
		frame.setVisible(true);

		gc = panel.getGraphics();
		gc.fillRect(0, 0, image.getWidth(), image.getHeight());
		gc.setColor(Color.BLACK);

		imageG = image.createGraphics();
		imageG.setBackground(Color.WHITE);
		imageG.fillRect(0, 0, image.getWidth(), image.getHeight());
		imageG.setColor(Color.BLACK);

		panel.addMouseMotionListener(new MouseMotionListener() {
			public void mouseDragged(MouseEvent m) {
				// the mouse(pen) was dragged, so the pixels at coords found in
				// MouseEvent m must be updated with current color
				Point p = m.getPoint();
				gc.fillOval(lastPos.x, lastPos.y, 16, 16);
				imageG.fillOval(lastPos.x, lastPos.y, 16, 16);
				lastPos = p;
				//JOptionPane.showMessageDialog(null, "My Goodness, this is so concise");
			}

			public void mouseMoved(MouseEvent m) {
			}
		});
		panel.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {
			}

			public void mousePressed(MouseEvent e) {
				lastPos = e.getPoint();
			}

			public void mouseReleased(MouseEvent e) {
				lastPos = null;
			}

			public void mouseEntered(MouseEvent e) {
			}

			public void mouseExited(MouseEvent e) {
			}
		});
	}
	
	private Instances getData(String filename, Integer posClass) throws IOException, URISyntaxException {
		File file = new File(KNearestNeighborAlgorithmsStarter.class.getResource(filename).toURI());
		BufferedReader inputReader = new BufferedReader(new FileReader(file));
		Instances data = new Instances(inputReader);
		data.setClassIndex(data.numAttributes() - posClass);
		return data;
	}
}
