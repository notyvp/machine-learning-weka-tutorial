package edu.rupp.mite.w13;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import edu.rupp.mite.w13.weka.algorithms.KNearestNeighborAlgorithm;

public class MainGUI {

	private JFrame frmInputKhmerNumber;
	private KNearestNeighborAlgorithm kNearestNeighborAlgorithm;
	private JTextPane textPane;
	private SimplePaintPanelGUI panel;
	private BufferedImage image;

	/**
	 * The color black.
	 */
	public static final Color BLACK = Color.BLACK;
	/**
	 * The color white.
	 */
	public static final Color WHITE = Color.WHITE;

	// default font
	private static final Font DEFAULT_FONT = new Font("Khmer UI", Font.PLAIN, 16);
	private static final Font DEFAULT_BIG_FONT = new Font("Khmer UI", Font.PLAIN, 130);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI window = new MainGUI();
					window.frmInputKhmerNumber.setVisible(true);
					window.kNearestNeighborAlgorithm = KNearestNeighborAlgorithm.getInstance();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmInputKhmerNumber = new JFrame();
		frmInputKhmerNumber.setResizable(false);
		frmInputKhmerNumber.setTitle("Input Khmer Number");
		frmInputKhmerNumber.setBounds(100, 100, 497, 390);
		frmInputKhmerNumber.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmInputKhmerNumber.getContentPane().setLayout(null);

		panel = new SimplePaintPanelGUI();
		panel.setBounds(12, 14, 320, 320);
		frmInputKhmerNumber.getContentPane().add(panel);

		this.textPane = new JTextPane();
		textPane.setFont(DEFAULT_BIG_FONT);
		textPane.setBounds(344, 197, 123, 137);
		StyledDocument doc = textPane.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);
		textPane.enable(false);
		// textPane.setText("១២៣៤៥៦៧៨៩០");
		frmInputKhmerNumber.getContentPane().add(textPane);

		JButton btnClean = new JButton("Claer");
		btnClean.addActionListener(new CleanActionListener(panel));
		btnClean.setBounds(344, 14, 125, 25);
		frmInputKhmerNumber.getContentPane().add(btnClean);

		JButton btnPrediction = new JButton("Prediction");
		btnPrediction.addActionListener(new PredictionActionListener(textPane, kNearestNeighborAlgorithm));
		btnPrediction.setBounds(344, 52, 125, 25);
		frmInputKhmerNumber.getContentPane().add(btnPrediction);

		JLabel lblNewLabel = new JLabel("១២៣៤៥៦៧៨៩០");
		lblNewLabel.setBounds(344, 159, 125, 25);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(DEFAULT_FONT);
		frmInputKhmerNumber.getContentPane().add(lblNewLabel);

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new SaveActionLionListener(frmInputKhmerNumber));
		btnSave.setBounds(344, 96, 125, 25);
		frmInputKhmerNumber.getContentPane().add(btnSave);
	}

	private class PredictionActionListener implements ActionListener {

		JTextPane textPane;
		KNearestNeighborAlgorithm kNearestNeighborAlgorithm;

		public PredictionActionListener(JTextPane textPane, KNearestNeighborAlgorithm kNearestNeighborAlgorithm) {
			this.kNearestNeighborAlgorithm = kNearestNeighborAlgorithm;
			this.textPane = textPane;
		}

		public void actionPerformed(ActionEvent arg0) {
			Integer predictioValue = 4;
			try {

				// predictioValue = this.kNearestNeighborAlgorithm.prediction(new
				// KhmerNumberInstances());
				switch (predictioValue) {
				case 0:
					this.textPane.setText("០");
					break;
				case 1:
					this.textPane.setText("១");
					break;
				case 2:
					this.textPane.setText("២");
					break;
				case 3:
					this.textPane.setText("៣");
					break;
				case 4:
					this.textPane.setText("៤");
					break;
				case 5:
					this.textPane.setText("៥");
					break;
				case 6:
					this.textPane.setText("៦");
					break;
				case 7:
					this.textPane.setText("៧");
					break;
				case 8:
					this.textPane.setText("៨");
					break;
				case 9:
					this.textPane.setText("៩");
					break;
				default:
					this.textPane.setText("?");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private class CleanActionListener implements ActionListener {
		JPanel panel;

		public CleanActionListener(JPanel panel) {
			this.panel = panel;
		}

		public void actionPerformed(ActionEvent arg0) {
			panel.repaint();
		}
	}

	private class SaveActionLionListener implements ActionListener {

		JFrame frmInputKhmerNumber;

		public SaveActionLionListener(JFrame frmInputKhmerNumber) {
			this.frmInputKhmerNumber = frmInputKhmerNumber;
		}

		/**
		 * This method cannot be called directly.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			FileDialog chooser = new FileDialog(frmInputKhmerNumber, "Use a .png or .jpg extension", FileDialog.SAVE);
			chooser.setVisible(true);
			String filename = chooser.getFile();
			if (filename != null) {
				save(chooser.getDirectory() + File.separator + chooser.getFile());
			}
		}

	}

	/**
	 * Saves the drawing to using the specified filename. The supported image
	 * formats are JPEG and PNG; the filename suffix must be {@code .jpg} or
	 * {@code .png}.
	 *
	 * @param filename the name of the file with one of the required suffixes
	 * @throws IllegalArgumentException if {@code filename} is {@code null}
	 */
	public void save(String filename) {
		validateNotNull(filename, "filename");
		panel.save(filename);

	}

	// throw an IllegalArgumentException if s is null
	private static void validateNotNull(Object x, String name) {
		if (x == null)
			throw new IllegalArgumentException(name + " is null");
	}
}
