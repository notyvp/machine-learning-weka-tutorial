% 
% 1. Title of Database: Optical Recognition of Handwritten Digits
% 
% 2. Source:
% 	E. Alpaydin, C. Kaynak
% 	Department of Computer Engineering
% 	Bogazici University, 80815 Istanbul Turkey
% 	alpaydin@boun.edu.tr
% 	July 1998
% 
% 3. Past Usage:
% 	C. Kaynak (1995) Methods of Combining Multiple Classifiers and Their
% 	Applications to Handwritten Digit Recognition, 
% 	MSc Thesis, Institute of Graduate Studies in Science and 
% 	Engineering, Bogazici University.
% 
% 	E. Alpaydin, C. Kaynak (1998) Cascading Classifiers, Kybernetika,
% 	to appear. ftp://ftp.icsi.berkeley.edu/pub/ai/ethem/kyb.ps.Z
% 
% 4. Relevant Information:
% 	We used preprocessing programs made available by NIST to extract
% 	normalized bitmaps of handwritten digits from a preprinted form. From
% 	a total of 43 people, 30 contributed to the training set and different
% 	13 to the test set. 32x32 bitmaps are divided into nonoverlapping 
% 	blocks of 4x4 and the number of on pixels are counted in each block.
% 	This generates an input matrix of 8x8 where each element is an 
% 	integer in the range 0..16. This reduces dimensionality and gives 
% 	invariance to small distortions.
% 
% 	For info on NIST preprocessing routines, see 
% 	M. D. Garris, J. L. Blue, G. T. Candela, D. L. Dimmick, J. Geist, 
% 	P. J. Grother, S. A. Janet, and C. L. Wilson, NIST Form-Based 
% 	Handprint Recognition System, NISTIR 5469, 1994.
% 
% 5. Number of Instances
% 	optdigits.tra	Training	3823
% 	optdigits.tes	Testing		1797
% 	
% 	The way we used the dataset was to use half of training for 
% 	actual training, one-fourth for validation and one-fourth
% 	for writer-dependent testing. The test set was used for 
% 	writer-independent testing and is the actual quality measure.
% 
% 6. Number of Attributes
% 	64 input+1 class attribute
% 
% 7. For Each Attribute:
% 	All input attributes are integers in the range 0..16.
% 	The last attribute is the class code 0..9
% 
% 8. Missing Attribute Values
% 	None
% 
% 9. Class Distribution
% 	Class:	No of examples in training set
% 	0:  376
% 	1:  389
% 	2:  380
% 	3:  389
% 	4:  387
% 	5:  376
% 	6:  377
% 	7:  387
% 	8:  380
% 	9:  382
% 
% 	Class: No of examples in testing set
% 	0:  178
% 	1:  182
% 	2:  177
% 	3:  183
% 	4:  181
% 	5:  182
% 	6:  181
% 	7:  179
% 	8:  174
% 	9:  180
% 
% Accuracy on the testing set with k-nn 
% using Euclidean distance as the metric
% 
%  k =  1   : 98.00
%  k =  2   : 97.38
%  k =  3   : 97.83
%  k =  4   : 97.61
%  k =  5   : 97.89
%  k =  6   : 97.77
%  k =  7   : 97.66
%  k =  8   : 97.66
%  k =  9   : 97.72
%  k = 10   : 97.55
%  k = 11   : 97.89

@relation optdigits

@attribute 'input1'  numeric
@attribute 'input2'  numeric
@attribute 'input3'  numeric
@attribute 'input4'  numeric
@attribute 'input5'  numeric
@attribute 'input6'  numeric
@attribute 'input7'  numeric
@attribute 'input8'  numeric
@attribute 'input9'  numeric
@attribute 'input10' numeric
@attribute 'input11' numeric
@attribute 'input12' numeric
@attribute 'input13' numeric
@attribute 'input14' numeric
@attribute 'input15' numeric
@attribute 'input16' numeric
@attribute 'input17' numeric
@attribute 'input18' numeric
@attribute 'input19' numeric
@attribute 'input20' numeric
@attribute 'input21' numeric
@attribute 'input22' numeric
@attribute 'input23' numeric
@attribute 'input24' numeric
@attribute 'input25' numeric
@attribute 'input26' numeric
@attribute 'input27' numeric
@attribute 'input28' numeric
@attribute 'input29' numeric
@attribute 'input30' numeric
@attribute 'input31' numeric
@attribute 'input32' numeric
@attribute 'input33' numeric
@attribute 'input34' numeric
@attribute 'input35' numeric
@attribute 'input36' numeric
@attribute 'input37' numeric
@attribute 'input38' numeric
@attribute 'input39' numeric
@attribute 'input40' numeric
@attribute 'input41' numeric
@attribute 'input42' numeric
@attribute 'input43' numeric
@attribute 'input44' numeric
@attribute 'input45' numeric
@attribute 'input46' numeric
@attribute 'input47' numeric
@attribute 'input48' numeric
@attribute 'input49' numeric
@attribute 'input50' numeric
@attribute 'input51' numeric
@attribute 'input52' numeric
@attribute 'input53' numeric
@attribute 'input54' numeric
@attribute 'input55' numeric
@attribute 'input56' numeric
@attribute 'input57' numeric
@attribute 'input58' numeric
@attribute 'input59' numeric
@attribute 'input60' numeric
@attribute 'input61' numeric
@attribute 'input62' numeric
@attribute 'input63' numeric
@attribute 'input64' numeric
@attribute 'class' {0,1,2,3,4,5,6,7,8,9}

@data
0,1,6,15,12,1,0,0,0,7,16,6,6,10,0,0,0,8,16,2,0,11,2,0,0,5,16,3,0,5,7,0,0,7,13,3,0,8,7,0,0,4,12,0,1,13,5,0,0,0,14,9,15,9,0,0,0,0,6,14,7,1,0,0,0
0,0,8,15,16,13,0,0,0,1,11,9,11,16,1,0,0,0,0,0,7,14,0,0,0,0,3,4,14,12,2,0,0,1,16,16,16,16,10,0,0,2,12,16,10,0,0,0,0,0,2,16,4,0,0,0,0,0,9,14,0,0,0,0,7
0,0,0,3,11,16,0,0,0,0,5,16,11,13,7,0,0,3,15,8,1,15,6,0,0,11,16,16,16,16,10,0,0,1,4,4,13,10,2,0,0,0,0,0,15,4,0,0,0,0,0,3,16,0,0,0,0,0,0,1,15,2,0,0,4
0,0,5,14,4,0,0,0,0,0,13,8,0,0,0,0,0,3,14,4,0,0,0,0,0,6,16,14,9,2,0,0,0,4,16,3,4,11,2,0,0,0,14,3,0,4,11,0,0,0,10,8,4,11,12,0,0,0,4,12,14,7,0,0,6
0,0,11,16,10,1,0,0,0,4,16,10,15,8,0,0,0,4,16,3,11,13,0,0,0,1,14,6,9,14,0,0,0,0,0,0,12,10,0,0,0,0,0,6,16,6,0,0,0,0,5,15,15,8,8,3,0,0,10,16,16,16,16,6,2
0,0,1,11,13,11,7,0,0,0,9,14,6,4,3,0,0,0,16,12,16,15,2,0,0,5,16,10,4,12,6,0,0,1,1,0,0,10,4,0,0,0,0,0,5,10,0,0,0,0,0,8,15,3,0,0,0,0,1,13,5,0,0,0,5
0,0,3,13,13,2,0,0,0,6,16,12,10,8,0,0,0,9,15,12,16,6,0,0,0,10,16,16,13,0,0,0,0,1,12,16,12,14,4,0,0,0,11,8,0,3,12,0,0,0,13,11,8,13,12,0,0,0,3,15,11,6,0,0,8
0,0,0,3,16,11,1,0,0,0,0,8,16,16,1,0,0,0,0,9,16,14,0,0,0,1,7,16,16,11,0,0,0,9,16,16,16,8,0,0,0,1,8,6,16,7,0,0,0,0,0,5,16,9,0,0,0,0,0,2,14,14,1,0,1
0,0,0,4,13,16,16,3,0,0,8,16,9,12,16,4,0,7,16,3,3,15,13,0,0,9,15,14,16,16,6,0,0,1,8,7,12,15,0,0,0,0,0,0,13,10,0,0,0,0,0,3,15,6,0,0,0,0,0,5,15,4,0,0,9
0,0,7,11,11,6,0,0,0,9,16,12,10,14,0,0,0,5,2,0,4,14,0,0,0,0,1,5,14,6,0,0,0,1,15,16,16,10,0,0,0,0,7,4,4,15,6,0,0,0,5,4,8,13,12,0,0,0,14,16,12,10,1,0,3

